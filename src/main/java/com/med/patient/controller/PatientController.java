package com.med.patient.controller;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.med.patient.dto.PatientDto;
import com.med.patient.entity.Patient;
import com.med.patient.service.PatientService;

/**
 * @author Rathakrishnan Duraimoni
 *
 */
@RestController
@RequestMapping("patient")
public class PatientController {

	@Autowired
	private PatientService patientServie;

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	public ResponseEntity<?> get(@PathVariable Long id) {
		try {
			PatientDto patientDto = modelMapper.map(patientServie.get(id), PatientDto.class);
			if (Optional.ofNullable(patientDto.getId()).isPresent()) {
				return new ResponseEntity<>(patientDto, HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Patient not present for this " + id, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @return
	 */
	@GetMapping()
	public ResponseEntity<?> getAll() {
		try {
			Type listType = new TypeToken<List<PatientDto>>() {
			}.getType();
			List<PatientDto> patientDtoLst = modelMapper.map(patientServie.getAll(), listType);
			if (Optional.ofNullable(patientDtoLst).get().size() == 0) {
				return new ResponseEntity<>(patientDtoLst, HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<>(patientDtoLst, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * @param PatientDto
	 * @return
	 */
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid PatientDto patientDto) {
		try {
			Patient patient = modelMapper.map(patientDto, Patient.class);
			PatientDto patientDtoR = modelMapper.map(patientServie.save(patient), PatientDto.class);
			return new ResponseEntity<PatientDto>(patientDtoR, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		try {
			String response = "Patient got deleted with Id :" + id;
			patientServie.delete(id);
			return new ResponseEntity<String>(response, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * @param PatientDto
	 * @return
	 */
	@PutMapping
	public ResponseEntity<PatientDto> update(@RequestBody PatientDto patientDto) {
		Patient patient = modelMapper.map(patientDto, Patient.class);
		PatientDto patientDtoR = modelMapper.map(patientServie.update(patient), PatientDto.class);
		return new ResponseEntity<PatientDto>(patientDtoR, HttpStatus.ACCEPTED);
	}

}