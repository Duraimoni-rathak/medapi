package com.med.patient.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.med.patient.entity.Patient;

@Repository
public interface PatientRepo extends JpaRepository<Patient, Long> {

}
