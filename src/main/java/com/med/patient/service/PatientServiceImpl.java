package com.med.patient.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.med.patient.entity.Patient;
import com.med.patient.repository.PatientRepo;

@Service
public class PatientServiceImpl implements PatientService {

	@Autowired
	private PatientRepo patientRepo;

	@Override
	public Patient get(Long id) {
		return patientRepo.findById(id).orElse(new Patient());
	}

	@Override
	public List<Patient> getAll() {
		return patientRepo.findAll();
	}

	@Override
	public Patient save(Patient patient) {

		return patientRepo.save(patient);
	}

	@Override
	public void delete(Long id) {
		patientRepo.deleteById(id);
	}

	@Override
	public Patient update(Patient patient) {
		return patientRepo.save(patient);
	}

}
