package com.med.patient.service;

import java.util.List;

import com.med.patient.entity.Patient;

/**
 * @author A575613
 *
 */
public interface PatientService {
	
	Patient get(Long id);

	List<Patient> getAll();

	Patient save(Patient patient);

	void delete(Long id);

	Patient update(Patient Patient);
}
