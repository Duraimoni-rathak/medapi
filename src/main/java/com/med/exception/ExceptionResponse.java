package com.med.exception;

import org.springframework.stereotype.Component;

@Component
public class ExceptionResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer code;

	private String message;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
