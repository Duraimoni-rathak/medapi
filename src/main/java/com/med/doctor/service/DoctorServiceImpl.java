package com.med.doctor.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.med.doctor.entity.Doctor;
import com.med.doctor.repository.DoctorRepo;

@Service
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	private DoctorRepo doctorRepo;

	@Override
	public Doctor get(Long id) {
		return doctorRepo.findById(id).orElse(new Doctor());
	}

	@Override
	public List<Doctor> getAll() {
		return doctorRepo.findAll();
	}

	@Override
	public Doctor save(Doctor doctor) {
		return doctorRepo.save(doctor);

	}

	@Override
	public void delete(Long id) {
		doctorRepo.deleteById(id);
	}

	@Override
	public Doctor update(Doctor doctor) {
		return doctorRepo.save(doctor);
	}

}
