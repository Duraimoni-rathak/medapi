package com.med.doctor.service;

import java.util.List;

import com.med.doctor.dto.DoctorDto;
import com.med.doctor.entity.Doctor;

public interface DoctorService {

	Doctor get(Long id);

	List<Doctor> getAll();

	Doctor save(Doctor doctor);

	void delete(Long id);

	Doctor update(Doctor doctor);

}
