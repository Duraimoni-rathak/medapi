package com.med.doctor.controller;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.med.doctor.dto.DoctorDto;
import com.med.doctor.entity.Doctor;
import com.med.doctor.service.DoctorService;

/**
 * @author Rthakrishnan Duraimoni
 *
 */
@RestController
@RequestMapping("doctor")
public class DoctorController {

	@Autowired
	private DoctorService doctorServie;

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("{id}")
	public ResponseEntity<?> get(@PathVariable Long id) {
		try {
			DoctorDto doctorDto = modelMapper.map(doctorServie.get(id), DoctorDto.class);
			System.out.println("doctorDto DTO ->" + doctorDto);
			if (Optional.ofNullable(doctorDto.getId()).isPresent()) {
				return new ResponseEntity<>(doctorDto, HttpStatus.OK);
			} else {
				return new ResponseEntity<>("No Value", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @return
	 */
	@GetMapping()
	public ResponseEntity<?> getAll() {
		try {
			Type listType = new TypeToken<List<DoctorDto>>() {
			}.getType();
			List<DoctorDto> doctorLst = modelMapper.map(doctorServie.getAll(), listType);
			if (Optional.ofNullable(doctorLst).get().size() == 0) {
				return new ResponseEntity<>(doctorLst, HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<>(doctorLst, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @param doctorDto
	 * @return
	 */
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid DoctorDto doctorDto) {
		try {
			Doctor doctor = modelMapper.map(doctorDto, Doctor.class);
			DoctorDto doctorDtor = modelMapper.map(doctorServie.save(doctor), DoctorDto.class);
			return new ResponseEntity<>(doctorDtor, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		try {
			doctorServie.delete(id);
			return new ResponseEntity<>("deleted", HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @param doctorDto
	 * @return
	 */
	@PutMapping
	public ResponseEntity<?> update(@RequestBody @Valid DoctorDto doctorDto) {
		try {
			Doctor doctor = modelMapper.map(doctorDto, Doctor.class);
			DoctorDto doctorDtor = modelMapper.map(doctorServie.save(doctor), DoctorDto.class);
			return new ResponseEntity<>(doctorDtor, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
