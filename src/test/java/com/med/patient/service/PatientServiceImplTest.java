package com.med.patient.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.med.doctor.entity.Doctor;
import com.med.patient.entity.Patient;
import com.med.patient.repository.PatientRepo;
import com.med.patient.service.PatientServiceImpl;

@SpringBootTest
@RunWith(SpringRunner.class)
class PatientServiceImplTest {

	Patient patient;

	@InjectMocks
	PatientServiceImpl patientServiceImpl;

	@Mock
	PatientRepo patientRepo;


	@BeforeEach
	void init() {
		Doctor doctor = new Doctor(1l, "Ratha", "4", "d.ratha@gmail.com");
		patient = new Patient(1l, "patSugu", 23, 989343, doctor);
	}

	@Test
	void testGet() {
		when(patientRepo.findById(1L)).thenReturn(Optional.of(patient));
		assertEquals(patientServiceImpl.get(1l).getName(), "patSugu");
	}

	@Test
	void testGetAll() {
		when(patientRepo.findAll()).thenReturn(Stream.of(patient, patient).collect(Collectors.toList()));
		assertEquals(2, patientServiceImpl.getAll().size());
	}

	@Test
	void testSave() {
		when(patientRepo.save(patient)).thenReturn(patient);
		assertEquals(patient, patientServiceImpl.save(patient));
	}

	@Test
	void testDelete() {
		patientServiceImpl.delete(1l);
		verify(patientRepo, times(1)).deleteById(1l);
	}

}
