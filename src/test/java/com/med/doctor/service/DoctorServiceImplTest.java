package com.med.doctor.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.med.doctor.entity.Doctor;
import com.med.doctor.repository.DoctorRepo;

@SpringBootTest
@RunWith(SpringRunner.class)
class DoctorServiceImplTest {

	@InjectMocks
	DoctorServiceImpl doctorServiceImpl;

	@Mock
	DoctorRepo doctorRepo;

	Doctor doctor;

	@BeforeEach
	void init() {
		doctor = new Doctor(1l, "Ratha", "4", "d.ratha@gmail.com");
	}

	@Test
	void testGet() {

		when(doctorRepo.findById(1L)).thenReturn(Optional.of(doctor));
		assertEquals(doctorServiceImpl.get(1l).getName(), "Ratha");
	}

	@Test
	void testGetAll() {
		when(doctorRepo.findAll()).thenReturn(Stream.of(doctor, doctor).collect(Collectors.toList()));
		assertEquals(2, doctorServiceImpl.getAll().size());
	}

	@Test
	void testSave() {
		when(doctorRepo.save(doctor)).thenReturn(doctor);
		assertEquals(doctor, doctorServiceImpl.save(doctor));
	}

	@Test
	void testDelete() {
		doctorServiceImpl.delete(1l);
		verify(doctorRepo, times(1)).deleteById(1l);
	}

}
